Buatlah repository dengan nama day-3-conditional-function-namaKamu
    - index.html
    - index.js

Kamu diminta untuk membuat sebuah function vending machine sederhana. Dimana pada function ini menerima 3 parameters yaitu:
- email
- money
- drinkChoice

Asumsi pada Vending Machine:
1. terdapat 3 jenis minuman, yaitu:
    - mineral water seharga 5000
    - cola seharga 7500
    - coffee seharga 12250
2. Jika value drinkChoice kosong maka default drinkChoice adalah coffee

Requirements:
- Jika inputan value email dan money kosong maka munculkan pesan "Please check your input"

- Jika inputan email kosong (empty string, null, undefined) maka munculkan pesan "Sorry can't process it until your email filled"

- Jika inputan email bukan bertipe String maka munculkan pesan "Invalid input"

- Jika inputan money kosong (null, undefined atau 0) maka munculkan pesan "Sorry can't process it until you put your money"

- Jika inputan money bukan bertipe number maka munculkan "Invalid input"

- Jika value money cukup untuk membeli pilihan minuman dari customer maka tampilkan pesan dengan format sebagai berikut:
"Welcome [email] to May's Vending Machine"
"Your choice is [drinkChoice], here's your drink"
"Your changes are [kembalian]"
"Thank you"

- Jika value money tidak cukup untuk membeli pilihan minuman apapun maka tampilkan pesan dengan format sebagai berikut:
"Welcome [email] to May's Vending Machine"
"Sorry, insufficient balance we can't process your [drinkChoice]"
"You need [kekurangan uang customer] more to buy this drink"
"Thank you"

```Javascript
// drive test
// case 1
// email ='' dan money = 30000
// expected output:
Sorry can't process it until your email filled


// case 2
// email = true dan money = 30000
Invalid input

// case 3
// email = 'g2academy@mail.com' dan money = 0
Sorry can't process it until you put your money

// case 4
// email = 'g2academy@mail.com' dan money = "10000"
Invalid input

// case 5
// email = 'g2academy@mail.com' dan money = 20000
Welcome g2academy@mail.com to May's Vending Machine
Your choice is coffee , here's your drink
Your change are 7750
Thank you

//case 5
// email = 'g2academy@mail.com' dan money = 5000 dan drinkChoice = 'cola'
Welcome g2academy@mail.com to May's Vending Machine
Sorry, insufficient balance we can't process your cola
You need 2500 more to buy this drink
Thank you
```

