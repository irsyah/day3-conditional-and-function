console.log('Hello saya di-print di console');

var angka = 15;
// typeof namaVariable => untuk mengetahui tipe data sebuah variabel
console.log(typeof angka);

var nama = "G2 Academy";
var username = 'g2academy';

console.log("nama: " + nama + " bertipe " + typeof nama);
// Template Literal
console.log(`Nama: ${nama} bertipe ${typeof nama}`);

console.log("username: " + typeof username);

var isTruth = true;
console.log("isTruth: " + typeof isTruth);

var money;
console.log(`typeof money ${typeof money}`); //undefined

var password = null;
console.log(`typeof password ${typeof password}`);


// CARA MEMBUAT VARIABLE DI JS
// var
var greeting = "Hello Batch May 2022";
var greeting = "Hi Javascript"; //re-declared
greeting = "Let's dance"; // re-assign

console.log(greeting);

// let
let email = "rofiq@mail.com";
// let email = "mala@mail.com"; // tidak bisa di re-declared
email = "mala@mail.com";

console.log(email);

// const
const status = "WNA";
// const status = "WNI"; // tidak bisa di re-declared
// status = "WNI"; // tidak bisa di re-assigned
console.log(status);

// CONDITIONAL
//JIKA COMPARISION === ini artinya dia menyamakan value + tipe datanya
// == ini artinya cuma menyamakan value saja

let age = 0;

// jika umur dibawah 17 maka tampilkan kamu belum berhak dapat ktp
// selain itu maka tampilkan kamu dapat ktp

// if (age === undefined || age === null || age === '') {
if (!age) {
    console.log("Tolong isi age dulu");
} else if (age < 17) { // undefined < 17 => false
    console.log("kamu belum berhak dapat ktp");
} else {
    console.log("kamu berhak dapat ktp");
}

// TRUTHY FALSY
// mengecek sebuah variabel memiliki value atau tidak, jika memiliki value maka dia akan bernilai TRUE, KECUALI variabel tersebut valuenya adalah UNDEFINED, NULL, EMPTY STRING, 0, FALSE dan NAN ini dianggap false

let phoneNumber = '081223r';

// if (phoneNumber) { //true
//     console.log('truthy: ada isinya');
// } else {
//     console.log('falsy: isinya bisa undefined, null, empty string, 0, false ataupun NaN');
// }

// TERNARY 
// condition ? jika_statement_true : jika_statement_false 

let gender = 'female';
let firstName = 'faza';
let umur = 17;

(gender === 'female' && umur < 40) ? console.log(`Miss ${firstName}`) 
 : (gender === 'female' && umur >= 40) ? console.log(`Mrs. ${firstName}`)
 : console.log(`Mr ${firstName}`)

 // SWITCH
 switch (umur) {
    case 17:
        console.log("umur saya 17");   
        break;
    case 18:
        console.log("umur saya 18");
        break;
    default:
        console.log("belum 17 tahun");
        break;
 }

 // FUNCTION
 // regular function
 function tambah(num1 = 0, num2 = 0) {

    let result = num1 + num2; 
    return result;
 }

 let hasil = tambah(10, 5); //undefined
 console.log(hasil + 5);

 console.log(tambah(15, 3));

 console.log(tambah());

 // anonymus function
 let pengurangan = function(num1, num2) {
    return num1 - num2;
 }

 console.log(pengurangan(5, 15));

 // arrow function
 let pembagian = (num1, num2) => {
    return num1/num2;
 }

 console.log(pembagian(50, 5));